﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JPosting.DATA.EF;
using Microsoft.AspNet.Identity;

namespace JPosting.UI.Controllers
{
    
    public class ApplicationsController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: Applications
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                var applications = db.Applications.Include(a => a.AspNetUsers).Include(a => a.OpenPositions);
                return View(applications.ToList());
            }
            else if (User.IsInRole("Manager"))
            {
                //list of applications in the location of that current user
                //where ManagerID for that loaction == userID
                var currentUser = User.Identity.GetUserId();
                var applications = db.Applications.Where(x => x.OpenPositions.Locations.ManagerID == currentUser).ToList();
                return View(applications);
            }
            else
            {
                //current employee's where the app is not declined
                //employees
                //list where UserID == UserID(aspnet.identity)
                var currentUser = User.Identity.GetUserId();
                var application = db.Applications.Where(x => x.UserID == currentUser).ToList();
                return View(application.Where(x => x.IsDeclined == false));
            }
        }

        // GET: Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Applications applications = db.Applications.Find(id);
            if (applications == null)
            {
                return HttpNotFound();
            }
            return View(applications);
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionsID", "OpenPositionsID");
            return View();
        }
        // Get
        public ActionResult Apply(int id)
        {
            Applications app = new Applications();
            app.OpenPositionID = id;
            app.ApplicationDate = DateTime.Now;
            app.UserID = User.Identity.GetUserId();
            app.IsDeclined = false;
            return View(app);
        }

        //post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Apply(Applications app,HttpPostedFileBase resumeUpload)
        {
            Applications application = new Applications();
            application.ApplicationDate = DateTime.Now;
            application.UserID = User.Identity.GetUserId();
            application.ResumeFilename = resumeUpload.FileName;
            application.IsDeclined = false;
            application.OpenPositionID = app.OpenPositionID;

            //if (ModelState.IsValid)
            //{
                #region File Upload

                //check and see if there was a file uploaded, if not, default to the noimage.jpg
                string file = "noResume.pdf";

                //if a file has been uploaded
                if (resumeUpload != null)
                {
                    //get the name
                    file = resumeUpload.FileName;

                    //get the extension
                    string ext = file.Substring(file.LastIndexOf('.')).ToLower();

                    //check extension against a list of "good" extensions
                    string goodExt = ".pdf";

                    //see if this file is a pdf
                    if (ext == goodExt)
                    {
                        //this is a valid photo

                        Session["SizeError"] = null;
                        //create a new file name using a quid()
                        //file = Guid.NewGuid() + ext;

                        //create a unique name for each resume upload
                        file = application.UserID + DateTime.Now.Year + ext;

                        //save file to server
                        resumeUpload.SaveAs((Server.MapPath("~/Content/Images/" + file)));


                    }//if contains extension

                    //no matter what happens we need to update that photo Url property of the new student  in the DB
                    application.ResumeFilename = file;

                }//end if student photo not null

                #endregion

                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}

            //return View(application);
        }

        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Applications applications, HttpPostedFileBase resumeUpload)
        {
            if (ModelState.IsValid)
            {
                #region File Upload

                //check and see if there was a file uploaded, if not, default to the noimage.jpg
                string file = "noResume.pdf";

                //if a file has been uploaded
                if (resumeUpload != null)
                {
                    //get the name
                    file = resumeUpload.FileName;

                    //get the extension
                    string ext = file.Substring(file.LastIndexOf('.')).ToLower();

                    //check extension against a list of "good" extensions
                    string goodExt = ".pdf";

                    //see if this file is a photo
                    if (ext == goodExt)
                    {
                        //this is a valid photo
                      
                            Session["SizeError"] = null;
                            //create a new file name using a quid()
                            //file = Guid.NewGuid() + ext;

                            //create a unique name for each resume upload
                            file = applications.UserID + DateTime.Now.Year + ext;

                            //save file to server
                            resumeUpload.SaveAs((Server.MapPath("~/Content/Images/" + file)));
                       

                    }//if contains extension

                    //no matter what happens we need to update that photo Url property of the new student  in the DB
                    applications.ResumeFilename = file;

                }//end if student photo not null

                #endregion

                db.Applications.Add(applications);
                db.SaveChanges();
                return View(applications);
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", applications.UserID);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionsID", "OpenPositionsID", applications.OpenPositionID);
            return View(applications);
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Applications applications = db.Applications.Find(id);
            if (applications == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", applications.UserID);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionsID", "OpenPositionsID", applications.OpenPositionID);
            return View(applications);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Applications applications)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applications).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", applications.UserID);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionsID", "OpenPositionsID", applications.OpenPositionID);
            return View(applications);
        }

        // GET: Applications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Applications applications = db.Applications.Find(id);
            if (applications == null)
            {
                return HttpNotFound();
            }
            return View(applications);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Applications applications = db.Applications.Find(id);
            db.Applications.Remove(applications);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
