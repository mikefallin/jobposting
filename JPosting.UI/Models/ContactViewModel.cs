﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JPosting.UI.Models
{
    public class ContactViewModel
    {
        [Required(ErrorMessage = "**Required Field**")]
        public string Name { get; set; }


        [Required(ErrorMessage = "**Required Field**")]
        public string Subject { get; set; }


        [Required(ErrorMessage = "**Required Field**")]
        public string Message { get; set; }

        [EmailAddress(ErrorMessage = "Enter a valid email address")]
        [Required(ErrorMessage = "**Required Field**")]
        public string Email { get; set; }
    }
}