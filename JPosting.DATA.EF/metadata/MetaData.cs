﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPosting.DATA.EF//.metadata
{

    #region Applications MetaData
    [MetadataType(typeof(ApplicationsMetadata))]
    public partial class Applications { }

    public class ApplicationsMetadata
    {
        //public int ApplicationID { get; set; }

        [Required(ErrorMessage ="**Required**")]
        [Display(Name = "Open Position")]
        public int OpenPositionID { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Application Completion Date")]
        public System.DateTime ApplicationDate { get; set; }

        [UIHint("MultilineText")]
        [Display(Name = "Manager Notes")]
        [StringLength(2000, ErrorMessage = "Manager notes must be 2000 characters or less")]
        public string ManagerNotes { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [Display(Name = "Action Pending")]
        public bool IsDeclined { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [Display(Name = "Resume")]
        [StringLength(75, ErrorMessage = "Resume filename must be 75 characters or less")]
        public string ResumeFilename { get; set; }
    }
    #endregion

    #region Open Positions
    [MetadataType(typeof(OpenPositionsMetadata))]
    public partial class OpenPositions { }

    public class OpenPositionsMetadata
    {
        //public int OpenPositionID { get; set; }
        public int PositionID { get; set; }
        public int LocationID { get; set; }
    }
    #endregion

    #region Locations
    [MetadataType(typeof(LocationsMetadata))]
    public partial class Locations { }

    public class LocationsMetadata
    {
        //public int LocationID { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [StringLength(10, ErrorMessage = "Store number must be 10 characters or less")]
        [Display(Name =("Store Number"))]
        public string StoreNumber { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [StringLength(50, ErrorMessage = "City name must be 50 characters or less")]
        public string City { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [StringLength(2, ErrorMessage = "State must be 2 letter abbreviation")]
        public string State { get; set; }

        [Required(ErrorMessage = "**Required**")]
        [StringLength(128, ErrorMessage = "Manager must be 75 characters or less")]
        public string ManagerID { get; set; }
    }
    #endregion

    #region Positions
    [MetadataType(typeof(PositionsMetadata))]
    public class PositionsMetadata
    {
        // public int PositionID { get; set; }
        [Required(ErrorMessage = "**Required**")]
        [StringLength(50, ErrorMessage = "Job title must be 50 characters or less")]
        [Display(Name = "Job Title")]
        public string Title { get; set; }

        [Display(Name = "Job Description")]
        public string JobDescription { get; set; }
    } 
    #endregion
}
