﻿using System.Web.Mvc;
using JPosting.UI.Models;
using System.Net.Mail;
using System.Net;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //post - sending our data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //check the contact object for validity
            if (ModelState.IsValid)
            {

                //Create a body for the email (words)
                string body = string.Format($"Name: {contact.Name}<br />Email: {contact.Email}<br />Subject: {contact.Subject}<br /><br />{contact.Message}");

                //Create and configure the mail message (letter)
                MailMessage msg = new MailMessage("admin@mikefallin.com", contact.Email, contact.Subject, body);

                //Configure the mail message object (envelope)
                msg.IsBodyHtml = true; //body of the message is HTML

                //msg.CC.Add("mikefallin1982@gmail.com");
                msg.Bcc.Add("hcmiket@gmail.com"); //blind CC
                msg.Priority = MailPriority.High; //make sure it gets sent to main inbox
                                                  //Create and configure the SMTP client (Mail person)
                SmtpClient client = new SmtpClient("mail.mikefallin.com");
                client.Credentials = new NetworkCredential("admin@mikefallin.com", "0734oioi!");//attaches the username and password to the client so we can send the email            
                                                                                                //client.Port = 8889; //the original is 25
                                                                                                //send the email
                using (client)
                {

                    try
                    {
                        client.Send(msg); //sending the mail message object
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was an error sending your message. Please email admin@mikefallin.com";
                        return View();
                    }
                }
                //send the user to the ContactConfirmation View
                //pass the contct object with it
                ViewBag.Confirmation = "We have your message, we will get back to you ASAP!";
                return View();
            }
            else
            {
                return View(); //the model isnt valid, return to form
            }

        }

    }
}
