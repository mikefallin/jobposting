﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JPosting.DATA.EF;
using Microsoft.AspNet.Identity;

namespace JPosting.UI.Controllers
{
    public class OpenPositionsController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: OpenPositions
        public ActionResult Index()
        {
            if (User.IsInRole("Manager"))
            {
                //lock down so manager role can only see open positions at there location
                var currentUserId = User.Identity.GetUserId();
                var position = db.OpenPositions.Include(i => i.Locations).Where(x => x.Locations.ManagerID == currentUserId).ToList();
                return View(position);
            }
            else
            {
                //employees and admin can see all open positions
                
                var openPositions = db.OpenPositions.Include(o => o.Locations).Include(o => o.Positions);
                return View(openPositions.ToList());
            }
        }

        // GET: OpenPositions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositions openPositions = db.OpenPositions.Find(id);
            if (openPositions == null)
            {
                return HttpNotFound();
            }
            return View(openPositions);
        }

        // GET: OpenPositions/Create
        public ActionResult Create()
        {
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber");
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title");
            return View();
        }

        // POST: OpenPositions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OpenPositionsID,PositionID,LocationID")] OpenPositions openPositions)
        {
            if (ModelState.IsValid)
            {
                db.OpenPositions.Add(openPositions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPositions.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPositions.PositionID);
            return View(openPositions);
        }

        // GET: OpenPositions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositions openPositions = db.OpenPositions.Find(id);
            if (openPositions == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPositions.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPositions.PositionID);
            return View(openPositions);
        }

        // POST: OpenPositions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OpenPositionsID,PositionID,LocationID")] OpenPositions openPositions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(openPositions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPositions.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPositions.PositionID);
            return View(openPositions);
        }

        // GET: OpenPositions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositions openPositions = db.OpenPositions.Find(id);
            if (openPositions == null)
            {
                return HttpNotFound();
            }
            return View(openPositions);
        }

        // POST: OpenPositions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OpenPositions openPositions = db.OpenPositions.Find(id);
            db.OpenPositions.Remove(openPositions);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
